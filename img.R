library(rARPACK)
library(ripa)
library(EBImage)
library(jpeg)
library(png)
library(rafalib)

imgg <- readImage("boaz.jpg")


r <- imagematrix(imgg, type = "grey") # convert image to grescale

# apply SVD to image

r.svd <- svd(r)

u <- r.svd$u
v <- r.svd$v
d <- diag(r.svd$d)
dim(d)

# Plot the magnitude of the singular values
sigmas = r.svd$d # diagonal matrix (the entries of which are known as singular values)
#plot(1:length(r.svd$d), r.svd$d, xlab="i-th r.svd$d", ylab="r.svd$d",  main="Singular Values for dog");

# first approximation
ug <- as.matrix(u[-1, 1])
vg <- as.matrix(v[-1, 1])
dg <- as.matrix(d[1, 1])
lg <- u1 %*% d1 %*% t(v1)
l1gg <- imagematrix(lg, type = "grey")
#plot(l1g, useRaster = TRUE)


# more approximation
depth <- 2
u5 <- as.matrix(u[, 1:depth])
v5<- as.matrix(v[, 1:depth])
d5 <- as.matrix(d[1:depth, 1:depth])
l5gg <- u5 %*% d5 %*% t(v5)


## Warning: Pixel values were automatically clipped because of range over.

#plot(lsg, useRaster = TRUE)
#display(l5g, method = "raster")


# more approximation
depth <- 10
u10 <- as.matrix(u[, 1:depth])
v10<- as.matrix(v[, 1:depth])
d10 <- as.matrix(d[1:depth, 1:depth])
l10 <- u10 %*% d10 %*% t(v10)
l10gg <- imagematrix(l10, type = "grey")

## Warning: Pixel values were automatically clipped because of range over.


par(mfrow=c(2,2))
#orignal picture

# plog rank 1
display(l1gg, method = "raster")

# plot rank 5

display(l5gg, "Rank 1",method = "raster")
#l5g <- imagematrix(l5, type = "grey")
#plot(lsg, useRaster = TRUE)
display(l10gg, method = "raster")
# plot original image
display(imgg, method = "raster") 

